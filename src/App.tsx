import React from 'react';
import { useAppDispatch, useAppSelector } from './hooks/redux';
import { USERS_GET_START } from './store/modules/users/reducers/users-reducers';


function App() {
  const dispatch = useAppDispatch();
  const { users, isLoading, error } = useAppSelector(state => state.users)

  const loadData = () => dispatch(USERS_GET_START())
  return <div>
    {isLoading && <div>Loading...</div>}
    {error && <div style={{color: 'red'}}>{error}</div>}
    {users.map(user => <li>{user.name}</li>)}
    <button onClick={loadData}>LOAD</button>
  </div>;
}

export default App;
