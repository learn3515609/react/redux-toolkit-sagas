import { createAction, createReducer, PayloadAction } from '@reduxjs/toolkit';
import { IUser } from '../../../../models/IUser';

type UserState = {
  users: IUser[];
  isLoading: boolean;
  error: string;
}

const initialState: UserState = {
  users: [],
  isLoading: false,
  error: '',
}

export const USERS_GET_START = createAction('user/GET_START')
export const USERS_GET_SUCCESS = createAction<IUser[]>('user/GET_SUCCESS')
export const USERS_GET_FAIL = createAction<string>('user/GET_FAIL')

const usersReducer = createReducer(initialState, {
  [USERS_GET_START.type]: (state) => {
    state.isLoading = true;
  },
  [USERS_GET_SUCCESS.type]: (state, action: PayloadAction<IUser[]>) => {
    state.isLoading = false;
    state.error = '';
    state.users = action.payload
  },
  [USERS_GET_FAIL.type]: (state, action: PayloadAction<string>) => {
    state.isLoading = false;
    state.error = action.payload;
  }
})

export default usersReducer;