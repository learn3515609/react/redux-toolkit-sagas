import { takeEvery, call, put } from 'redux-saga/effects';
import { IUser } from '../../../../models/IUser';
import { USERS_GET_START, USERS_GET_SUCCESS, USERS_GET_FAIL } from '../reducers/users-reducers';

function* usersGetStartWorker() {
  try {
    const data: Response = yield call(fetch, 'https://jsonplaceholder.typicode.com/users');
    const users: IUser[] = yield call([data, data.json])
    yield put({
      type: USERS_GET_SUCCESS.type,
      payload: users
    })
  } catch(e) {
   if(e instanceof Error) {
      yield put({
        type: USERS_GET_FAIL.type,
        payload: e.message
      })
    }
  }

}

export function* usersSagaWatcher() {
  yield takeEvery(USERS_GET_START.type, usersGetStartWorker)
}


// try {
//   dispatch(USERS_GET_START())
//   const users: IUser[] = await fetch('https://jsonplaceholder.typicode.com/users')
//     .then(data => data.json());
//   dispatch(USERS_GET_SUCCESS(users))
// } catch (e) {
//   if(e instanceof Error) {
//     dispatch(USERS_GET_FAIL(e.message))
//   }
// }