import { combineReducers } from '@reduxjs/toolkit';
import usersReducer from './modules/users/reducers/users-reducers';

export const rootReducer = combineReducers({
  users: usersReducer
})