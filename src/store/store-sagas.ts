import { fork } from 'redux-saga/effects';
import { usersSagaWatcher } from './modules/users/sagas/users-sagas';

export function* rootSaga() {
  yield fork(usersSagaWatcher);
}